package project.filesystem;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;

public class MemoryFileSystemInstance extends FileSystemInstance {

    private final FileSystem fs;
    private final Path root;

    MemoryFileSystemInstance(String key){
        super(key);
        this.fs = Jimfs.newFileSystem(key, Configuration.unix());
        this.root = this.fs.getPath(key);
        try{
            Files.createDirectory(root);
        } catch (IOException ignore) {}
    }

    @Override
    public Path open(){
        return root;
    }

    @Override
    public void close() throws IOException {
        fs.close();
    }

    @Override
    public Path getPath(String parent, String name) {
        return fs.getPath(parent, name);
    }

    @Override
    public byte[] export(Path path) throws IOException {
        return super.export(path, this.fs.getPath("/output"));
    }
}
