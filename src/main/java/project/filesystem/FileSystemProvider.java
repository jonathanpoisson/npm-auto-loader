package project.filesystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class FileSystemProvider {


    private final boolean inMemory;

    @Autowired
    public FileSystemProvider(Environment environment) {
        this.inMemory = !environment.containsProperty("physical-storage");
    }

    public FileSystemInstance getInstance(){
        final String key = String.valueOf(System.currentTimeMillis());
        return getInstance(key);
    }

    public FileSystemInstance getInstance(String key){
        return inMemory ? new MemoryFileSystemInstance(key) : new PhysicalFileSystemInstance(key);
    }
}
