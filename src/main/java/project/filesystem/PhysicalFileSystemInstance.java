package project.filesystem;

import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PhysicalFileSystemInstance extends FileSystemInstance {

    private Path root;

    PhysicalFileSystemInstance(String key) {
        super(key);
    }

    @Override
    public Path open() throws IOException {
        this.root = Files.createTempDirectory(key);
        return this.root;
    }

    @Override
    public void close() throws IOException {
        FileUtils.deleteDirectory(root.toFile());
        super.close();
    }
}
