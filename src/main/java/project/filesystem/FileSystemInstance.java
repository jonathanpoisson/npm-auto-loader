package project.filesystem;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import project.utils.ZipUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

public abstract class FileSystemInstance {

    protected final String key;

    /***
     * Name of the top level directory for this instance
     *
     * @param key Unique Filename
     */
    FileSystemInstance(String key){
        this.key = key;
    }


    /**
     * Returns the unique key folder name for this instance
     * @return File name
     */
    public String getKey() {
        return key;
    }


    /**
     * Opens a new file system instance and returns a key path
     * @return Path of the new file system key
     */
    public abstract Path open() throws IOException;

    /**
     * Closes the file system instance
     */
    public void close() throws IOException {}

    /**
     * Returns a Path representing a file within a parent.
     * Various file systems should override this with their own implementation.
     *
     * @param parent Absolute path to the parent directory.
     * @param name Name of the file
     * @return Path of the new file within the parent directory.
     */
    public Path getPath(String parent, String name){
        return Paths.get(parent, name);
    }

    /**
     * Creates a new file with provided name under provided parent directory with Binary or Text content
     * @param parent Parent of the new file
     * @param name Name for the new file
     * @param content Binary content for the new file
     */
    public Path createFile(Path parent, String name, Object content) throws IOException {
        // Location of the new file
        final Path path = getPath(parent.toString(), name);

        if(!Files.exists(path)) {
            // Ensure full path exists
            createDirectories(parent);
            // Write to the file
            writeFile(path, content);
        }
        return path;
    }

    private Path writeFile(Path path, Object content) throws IOException {
        Files.createFile(path);
        return content instanceof byte[] ?
                writeFile(path, (byte[]) content) :
                writeFile(path, String.valueOf(content));
    }

    private Path writeFile(Path path, byte[] content) throws IOException {
        return Files.write(path, content);
    }

    private Path writeFile(Path path, String content) throws IOException {
        return Files.write(path, Collections.singletonList(content));
    }

    /**
     * Creates all missing directories found in the provided path.
     *
     * @param directory Full path to target directory
     * @throws IOException Failed to create directories
     */
    private void createDirectories(Path directory) throws IOException {
        if(!Files.exists(directory)) {
            Files.createDirectories(directory);
        }
    }

    /**
     * Exports the provided key directory as a zipped byte[]
     * @param path Root Directory
     * @return byte[] of the resulting zip file
     * @throws IOException if key or output are not accessible
     */
    public byte[] export(Path path) throws IOException {
        final Path output = Paths.get(
                FileUtils.getTempDirectory().toString(),
                "output-" + System.currentTimeMillis()
        );
        return export(path, output );
    }

    /**
     * Exports the provided key directory as a zipped byte[]
     * @param root Root Directory
     * @param output Location of the output zip
     * @return byte[] of the resulting zip file
     * @throws IOException if key or output are not accessible
     */
    protected byte[] export(Path root, Path output) throws IOException {
        // Create the output file if required
        if(!Files.exists(output)){
            Files.createFile(output);
        }
        // Generate zip from key file into output
        ZipUtils.createZip(root, output);
        // Read output content
        final byte[] content = IOUtils.toByteArray(Files.newInputStream(output));
        // Delete the output file
        Files.delete(output);
        return content;
    }

}
