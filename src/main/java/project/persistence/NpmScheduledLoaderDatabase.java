package project.persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import project.model.jdbc.ScheduledDependency;

import javax.annotation.PostConstruct;
import java.util.List;

import static project.model.jdbc.ScheduledDependencyRowMapper.INSTANCE;
import static project.model.jdbc.ScheduledDependencyTable.*;

@Service
public class NpmScheduledLoaderDatabase {

    private static final Logger logger = LoggerFactory.getLogger(NpmScheduledLoaderDatabase.class.getName());

    private final JdbcTemplate jdbcTemplate;

    public NpmScheduledLoaderDatabase(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ScheduledDependency> getDependencies(){
        return jdbcTemplate.query(SELECT_ALL, INSTANCE);
    }

    public boolean putDependency(String name){
        try {
            return jdbcTemplate.update(INSERT, name, System.currentTimeMillis()) > 0;
        }catch (DataAccessException e){
            logger.error("Failed to persist dependency: " + name, e);
            return false;
        }
    }

    public boolean deleteDependency(String name){
        try {
            return jdbcTemplate.update(DELETE, name) > 0;
        }catch (DataAccessException e){
            logger.error("Failed to delete dependency: " + name, e);
            return false;
        }
    }


    @PostConstruct
    public void init(){
        this.jdbcTemplate.execute(CREATE_TABLE);
    }

}
