package project.storage;

import project.filesystem.FileSystemInstance;

import java.io.IOException;
import java.nio.file.Path;

public class NodeSassBinariesStorageHandler extends StorageHandler {

    private final Path root;

    public NodeSassBinariesStorageHandler(FileSystemInstance instance) throws IOException {
        super(instance);
        this.root = open();
    }

    public Path addFile(String name, byte[] content) throws IOException {
        return getFileSystem().createFile(root, name, content);
    }

    public byte[] export() throws IOException {
        return super.export(root);
    }
}
