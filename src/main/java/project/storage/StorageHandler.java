package project.storage;

import project.filesystem.FileSystemInstance;

import java.io.IOException;
import java.nio.file.Path;

public abstract class StorageHandler {

    private final FileSystemInstance instance;

    StorageHandler(FileSystemInstance instance) {
        this.instance = instance;
    }

    FileSystemInstance getFileSystem() {
        return instance;
    }

    public String getKey(){
        return this.instance.getKey();
    }

    public Path open() throws IOException {
        return this.instance.open();
    }

    public byte[] export(Path path) throws IOException {
        return this.instance.export(path);
    }




}
