package project.storage;

import project.filesystem.FileSystemInstance;
import project.model.NpmPackage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class NpmPackageStorageHandler extends StorageHandler {

    private final Path root;

    public NpmPackageStorageHandler(FileSystemInstance instance) throws IOException {
        super(instance);
        this.root = open();
    }

    /**
     * Adds a given Npm package entry in the storage filesystem.
     *
     * @param p Current NpmPackage
     * @param json Content of the package.json for the depedency (not dependency[version])
     * @param tarball Binary content of the tarball of the dependency[version]
     * @throws IOException Can happen for unknown reasons, mostly bad paths.
     */
    public void addFile(NpmPackage p, String json, byte[] tarball) throws IOException {

        // Create dependency directory
        final Path basePath = getFileSystem().getPath(root.toString(), p.getName());
        if(!Files.exists(basePath)) {
            Files.createDirectories(basePath);
        }
        // Write package.json in /storage/<name>/package.json
        getFileSystem().createFile(basePath, "package.json", json);

        // Write the tarball in /storage/<name>/<name>-<version.tgz (sub names are truncated)
        String location = (p.getName() + "-" + p.getVersion()) + ".tgz";
        if(p.getName().contains("/")){
            location = location.substring(location.indexOf("/") + 1);
        }
        getFileSystem().createFile(basePath, location, tarball);
    }

    public byte[] export() throws IOException {
        final byte[] content = super.export(root);
        getFileSystem().close();
        return content;
    }

}
