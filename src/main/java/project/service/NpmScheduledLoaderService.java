package project.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import project.model.jdbc.ScheduledDependency;
import project.persistence.NpmScheduledLoaderDatabase;

import java.util.List;
import java.util.Set;

@Service
public class NpmScheduledLoaderService {

    private final NpmScheduledLoaderDatabase storageHandler;

    private boolean isActive = true;

    public NpmScheduledLoaderService(NpmScheduledLoaderDatabase storageHandler) {
        this.storageHandler = storageHandler;
    }

    public boolean isActive(){
        return isActive;
    }

    public void start(){
        this.isActive = true;
        this.loadDependencies();
    }

    public void pause(){
        this.isActive = false;
    }

    @Scheduled(fixedDelay = 5000)
    public void loadDependencies(){
        final List<ScheduledDependency> dependencies = storageHandler.getDependencies();
        if(isActive() && !dependencies.isEmpty()){
            System.out.println(dependencies);

        }
    }
}
