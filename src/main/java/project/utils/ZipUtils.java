package project.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {

    private static final Logger logger = Logger.getLogger(ZipUtils.class.getName());

    /**
     * Takes a path to a Java NIO file and outputs a zipped representation
     * in the provided output directory.
     *
     * @param srcDir Path to a NIO file
     * @param outputDir Path to a NIO file
     * @throws IOException if either path do not exist
     */
    public static void createZip(Path srcDir, Path outputDir) throws IOException {
        try(final OutputStream fos = Files.newOutputStream(outputDir);
            final ZipOutputStream zos = new ZipOutputStream(fos)){
            final DirectoryStream<Path> directoryStream = Files.newDirectoryStream(srcDir);
            directoryStream.forEach(path -> addToZipFile(srcDir, path, zos));
            directoryStream.close();
        }
    }

    /**
     * Creates a new ZipEntry from a provided NIO path and
     * adds it to a provided ZipOutSteam.
     *
     * If the Path is a directory, the children Paths will be added
     * recursively.
     *
     * @param path Path to a NIO file or directory
     * @param zos Existing ZipOutputSteam
     */
    private static void addToZipFile(Path src, Path path, ZipOutputStream zos) {
        if(Files.isDirectory(path)){
            try(final Stream<Path> fileStream = Files.list(path)){
                fileStream.forEach(
                  child -> addToZipFile(src, child, zos)
                );
            } catch (IOException e) {
                logger.info("Failed to list files in path: " + path.toString() + " Error: " + e.getMessage());
            }
        }else{
            addEntry(src, path, zos);
        }
    }

    /**
     * Creates a new ZipEntry from a provided NIO path and
     * adds it to a provided ZipOutSteam.
     *
     * The provided Path MUST be a file and not a directory.
     *
     * @param path Path to a NIO file
     * @param zos Existing ZipOutputSteam
     */
    private static void addEntry(Path src, Path path, ZipOutputStream zos) {
        try(final InputStream is = Files.newInputStream(path)) {
            final String relPath = path.toString().substring(src.toString().length() + 1);
            final byte[] content = IOUtils.toByteArray(is);
            zos.putNextEntry(new ZipEntry(relPath));
            zos.write(content);
            zos.closeEntry();
        } catch (IOException e) {
            logger.info("Failed to write in path: " + path.toString() + " Error: " + e.getMessage());
        }
    }

}
