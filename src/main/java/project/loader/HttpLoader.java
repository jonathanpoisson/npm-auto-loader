package project.loader;

import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

abstract class HttpLoader {

    // Http Client
    private final CloseableHttpClient httpclient = HttpClients.custom()
            .setMaxConnTotal(10)
            .setMaxConnPerRoute(10)
//            .setProxy(new HttpHost("proxy.dmz.ava.local", 3128, "http"))
            .setDefaultRequestConfig(
                RequestConfig.custom()
                    .setSocketTimeout(10000)
                    .setConnectTimeout(10000)
                    .setCookieSpec(CookieSpecs.STANDARD)
                    .build()
            )
            .build();


    /**
     * Returns the base url of a given dependency
     * example: @angular/cli:1.6.5 --> NPM_REGISTRY_URL + @angular%2Fcli
     * @param dependency full name and version of a dependency
     * @return String base url to a dependency
     */
    String getBaseUrl(String dependency) {
        final String[] parts = dependency.split(":");
        String name = parts[0].replace("/", "%2F");
        if(name.contains("#")){
            // Because this is a thing somehow:
            // fsevents:pipobscure/fsevents#7dcdf9fa3f8956610fd6f69f72c67bace2de7138
            name = name.substring(parts[0].indexOf("#"));
        }
        return getRegistryUrl()  + name;
    }

    /**
     * Convert the github dependency to a direct link to the
     * github repo/commit so we can grab the actual version.
     *
     * Whoever thought this was a good idea should step on
     * several legos and reconsider their career choice.
     *
     * @param requested stupid github versioning string
     * @return URL to the stupid github package.json
     */
    String getGithubUrl(String requested){
        final String path;
        // Strip off the optional .git ending
        String dependency = requested.replace(".git", "");
        // Strip off the dependency name
        dependency = dependency.split(":",2)[1];
        if(dependency.contains("github:")){
            // Github commits -->  github:repo/name#commit
            path = dependency.substring("github:".length());
        }else if(dependency.contains("//github.com")){
            // Github repo -->  git+https://github.com/repo/name.git#commit
            final int start = dependency.indexOf("//github.com/") + "//github.com/".length();
            path = dependency.substring(start);
        }else{
            path = dependency;
        }

        final String[] parts = path.split("#");
        final String name = tryEncode(parts[0]);
        final String commit = parts.length > 1? tryEncode(parts[1]) : "master";
        return String.format("https://raw.githubusercontent.com/%s/%s/package.json", name, commit);
    }

    /**
     * Returns a default or custom NPM registry URL
     * @return String
     */
    String getRegistryUrl(){
        return  System.getProperty(
                "npm.registry.url", "https://registry.npmjs.org/"
        );
    }

    /**
     * Tries to encode a path, returns the input if there's an exception.
     * @param name path to encode
     * @return String
     */
    String tryEncode(String name){
        try{
            return URLEncoder.encode(name, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            return name;
        }
    }

    public CloseableHttpClient getHttpclient() {
        return httpclient;
    }
}
