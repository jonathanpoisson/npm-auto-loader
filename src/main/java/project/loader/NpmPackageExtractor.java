package project.loader;

import com.github.yuchi.semver.Range;
import com.github.yuchi.semver.Version;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import project.model.NpmPackage;

import java.util.*;

/**
 * Extracts all necessary values from the base package.json (baseRegistryUrl/<package>)
 * using the provided dependency string for version checking.
 *
 * if the dependency does not have a version, 'latest' will be selected (from 'dist-tags')
 */
public class NpmPackageExtractor {

    public static NpmPackage extract(String dependency, JsonObject json, int additionalVersions){
        // Name of the package
//        final String name = json.get("name").getAsString();

        // Find requested version (latest or matching NPM syntax)
        final JsonObject versions = json.get("versions").getAsJsonObject();
        final String[] parts = dependency.split(":", 2);
        final String name = parts[0];
        final String version = (parts.length == 1 || parts[1].equals("latest")) ?
            json.get("dist-tags").getAsJsonObject().get("latest").getAsString() :
            calculateVersion(parts[1], versions.keySet());

        // Dependencies of the version
        final JsonObject packageVersion = versions.get(version).getAsJsonObject();
        final Set<String> dependencies = getDependencies(packageVersion);
        final String tarball = getTarball(packageVersion);

        // Looks for latest 'numVersions' versions of this dependency
        final int numVersions = Math.min(additionalVersions, versions.size() -1);
        if(numVersions > 0) {
            dependencies.addAll(getLatestDependencies(versions, version, numVersions));
        }
        return new NpmPackage(name, version, dependencies, tarball);
    }

    /**
     *
     * @param versions List of versions for a given dependencies
     * @param selected The currently selected version (skip if within range)
     * @param numVersions Number of versions from the latest to fetch
     * @return Set of additional dependencies to fetch
     */
    private static Set<String> getLatestDependencies(JsonObject versions, String selected, int numVersions) {
        final Set<String> dependencies = new HashSet<>(numVersions);
        // versions are already sorted by earliest --> latest (LinkedTreeSet)
        final List<String> versionKeys = Lists.reverse(new ArrayList<>(versions.keySet()));
        // Grab the latest X while skipping the selected version
        int counter = 0;
        for(final String version : versionKeys){
            if(!version.equals(selected)) {
                final String name = versions.get(version).getAsJsonObject()
                        .get("name").getAsString();
                dependencies.add(name + ":" + version);
                counter++;
            }
            if(counter == numVersions){
                break;
            }
        }
        return dependencies;
    }

    /**
     * Calculates the optimal version to use based on the requested
     * version. It picks the highest-rated version according to
     * NPM's insane semantic versioning rules...
     *
     * @param requested Version provided after the name (@angular/cli:^1.50 || >=1.6.5)
     * @param versions Set of available versions.
     * @return version satisfying the requested semantic version requested
     */
    private static String calculateVersion(String requested, Set<String> versions){
        String matching = null;

        final Range req = Range.from(requested, true);

        if(versions.size() == 0){
            return requested;
        }

        // versions are already sorted by earliest --> latest (LinkedTreeSet)
        final List<String> sortedVersions = Lists.reverse(new ArrayList<>(versions));

        if(req != null) {
            for (String versionExp : sortedVersions) {
                Version version = Version.from(versionExp, true);
                if (version != null && req.test(version)) {
                    if (matching == null) {
                        matching = versionExp;
                    } else {
                        Version matched = Version.from(matching, true);
                        if (version.compareTo(matched) > 0) {
                            matching = versionExp;
                        }
                    }
                }
            }
        }
        return matching == null ? sortedVersions.get(0) : matching;
    }

    /**
     * Returns all dependencies from the package.json
     * @param json package.json of the selected version
     * @return Set<String> of all dependencies
     */
    private static Set<String> getDependencies(JsonObject json){
        final JsonElement element = json.get("dependencies");
        return element == null ?
                Sets.newHashSetWithExpectedSize(0) : extractDependencies(element.getAsJsonObject());
    }

    /**
     * Returns the tarball entry from the package.json
     * @param json package.json of the selected version
     * @return String url of the tarball for the selected version
     */
    private static String getTarball(JsonObject json){
        final JsonObject dist = json.get("dist").getAsJsonObject();
        return dist.get("tarball").getAsString();
    }

    /**
     *  Returns a set of dependencies by combining key:value entries
     * @param json package.json of the selected version
     * @return Set<String> of all dependencies
     */
    private static Set<String> extractDependencies(JsonObject json){
        final Set<String> dependencies = new HashSet<>(json.size());
        for(Map.Entry<String, JsonElement> entry : json.entrySet()) {
            // Add the exact dependency required
            dependencies.add(entry.getKey() + ":" + entry.getValue().getAsString());
        }
        return dependencies;
    }

}
