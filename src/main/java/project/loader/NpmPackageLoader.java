package project.loader;

import com.google.common.base.Charsets;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import project.model.*;
import project.storage.NpmPackageStorageHandler;

import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class NpmPackageLoader extends HttpLoader {

    // Used to marshall package.json response into a JsonObject
    private static final Gson gson = new Gson();

    // Used to communicate messages back to the UI
    private final ConcurrentLinkedQueue<Update> messageQueue;

    // Holds completed dependencies (avoid duplicate calls)
    private final Set<String> requestedDependencies = new HashSet<>();

    // In-memory filesystem using the Verdaccio file structure
    private final NpmPackageStorageHandler storageHandler;

    // Stack of dependencies to be loaded
    private final Deque<String> dependencyQueue = new LinkedList<>();

    public NpmPackageLoader(
            NpmPackageStorageHandler storageHandler,
            ConcurrentLinkedQueue<Update> messageQueue){
        this.storageHandler = storageHandler;
        this.messageQueue = messageQueue;
    }

    // Starts the dependency tree exploration
    public void start(final DependencyRequest request){
        final Set<String> originalDependencies = request.getDependencies();
        this.dependencyQueue.addAll(originalDependencies);

        while(!dependencyQueue.isEmpty()){
            final String dependency = dependencyQueue.poll();
            // Only grab additional dependencies for top-level
            final int numVersions = originalDependencies.contains(dependency)?
                    request.getNumVersions() : 0;
            executeRequest(dependency, numVersions);
        }
        this.requestedDependencies.clear();
    }

    // Recursively fetches dependencies and adds them to the storage handler.
    private void executeRequest(String dependency, int numVersions){
        if(!requestedDependencies.contains(dependency)) {

            if(dependency.contains(":github:") || dependency.contains("//github.com")){
                final HttpGet request = new HttpGet(getGithubUrl(dependency));
                try(CloseableHttpResponse response = getHttpclient().execute(request)) {
                    if (response.getStatusLine().getStatusCode() != 200) {
                        sendErrorResponse(dependency, request, response);
                        return;
                    }
                    final String githubJsonPackage = IOUtils.toString(response.getEntity().getContent(), Charsets.UTF_8);
                    final JsonObject json = gson.fromJson(githubJsonPackage, JsonObject.class);
                    this.requestedDependencies.add(dependency);
                    dependency = json.get("name").getAsString() + ":" + json.get("version").getAsString();

                }catch (Throwable e) {
                    e.printStackTrace();
                    this.messageQueue.add(new ErrorUpdate(dependency, e.getMessage()));
                }
            }

            // This returns the key json ( ../version fails for @scoped)
            final HttpGet request = new HttpGet(getBaseUrl(dependency));
            try(CloseableHttpResponse response = getHttpclient().execute(request)){
                if(response.getStatusLine().getStatusCode() != 200){
                    sendErrorResponse(dependency, request, response);
                    return;
                }

                // Extract NPM Package
                final String packageJson = IOUtils.toString(response.getEntity().getContent(), Charsets.UTF_8);
                final NpmPackage p = NpmPackageExtractor.extract(
                        dependency, gson.fromJson(packageJson, JsonObject.class), numVersions);

                final HttpGet tarbalReq = new HttpGet(p.getTarball());
                try(CloseableHttpResponse tarballResponse = getHttpclient().execute(tarbalReq)){
                    if(tarballResponse.getStatusLine().getStatusCode() != 200){
                        sendErrorResponse(dependency, request, response);
                        return;
                    }
                    byte[] tarball = IOUtils.toByteArray(tarballResponse.getEntity().getContent());
                    storageHandler.addFile(p, packageJson, tarball);
                }catch (Throwable e){
                    e.printStackTrace();
                    this.messageQueue.add(new ErrorUpdate(dependency, e.getMessage()));
                }

                final String resolved = p.getName() + ":" + p.getVersion();
                this.requestedDependencies.add(dependency);
                this.requestedDependencies.add(resolved);

                final LoadingUpdate update = new LoadingUpdate(dependency, resolved);
                this.messageQueue.add(update);

                // Scan and request transitive dependencies
                final Set<String> transDependencies = Sets.newHashSet(p.getDependencies());
                transDependencies.removeAll(this.dependencyQueue);
                transDependencies.removeAll(this.requestedDependencies);
                this.dependencyQueue.addAll(transDependencies);
            } catch (Throwable ex) {
                ex.printStackTrace();
                final ErrorUpdate update = new ErrorUpdate(dependency, ex.getMessage());
                this.messageQueue.add(update);
            }
        }
    }

    /**
     * Builds a response error for all network-related failures
     * @param dependency Requested dependency
     * @param request HttpRequest that failed
     * @param response HttpResponse of the failed call
     */
    private void sendErrorResponse(String dependency, HttpGet request, HttpResponse response){
        final String error = request.getURI().getPath() +" : " + response.getStatusLine();
        this.messageQueue.add(new ErrorUpdate(dependency, error));
    }
}
