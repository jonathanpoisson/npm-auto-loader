package project.loader;

import com.google.common.collect.Sets;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class NodeSassBinariesLoader extends HttpLoader {

    private static final String NODE_SASS_BINARIES_URL = "https://github.com/sass/node-sass/releases/tag/v";
    private static final String NODE_SASS_DOWNLOAD_PREFIX = "https://github.com/sass/node-sass/releases/download/";

    private final Set<String> prefixes = Sets.newHashSet("win", "linux-x64");

    public Set<String> getDownloadLinks(String version){
        try {
            final Document document = Jsoup.parse(URI.create(NODE_SASS_BINARIES_URL + String.valueOf(version)).toURL(), 10000);
            final Elements linkElems = document.select(".Details-element a");
            final List<String> links = new LinkedList<>();
            for (Element linkElem : linkElems) {
                String link = linkElem.attr("abs:href");
                if (isLinkValid(link)) {
                    links.add(link);
                }
            }
            return new HashSet<>(links);
        } catch (MalformedURLException e) {
            throw new RestClientException("Invalid URL provided: " + e.getMessage());
        } catch (IOException e) {
            throw new RestClientException("Failed to load Github binaries HTML: " + e.getMessage());
        }
    }

    public byte[] downloadLink(String link){
        final HttpGet request = new HttpGet(link);
        try(CloseableHttpResponse response = getHttpclient().execute(request)) {
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RestClientException("Invalid status from URL: " + link + " Status: " + response.getStatusLine().getReasonPhrase());
            }
            return IOUtils.toByteArray(response.getEntity().getContent());
        } catch (Throwable e) {
            throw new RestClientException("Exception thrown while loading URL: " + link + " Error: " + e.getMessage());
        }
    }

    private boolean isLinkValid(String link){
        boolean flag = false;
        if(link != null && link.startsWith(NODE_SASS_DOWNLOAD_PREFIX)){
            for(String prefix : prefixes){
                if(link.contains(prefix)){
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }
}
