package project.controller;

import org.springframework.web.bind.annotation.*;
import project.model.jdbc.ScheduledDependency;
import project.persistence.NpmScheduledLoaderDatabase;
import project.service.NpmScheduledLoaderService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/scheduler")
public class NpmScheduledLoaderController {

    private final NpmScheduledLoaderService loaderService;
    private final NpmScheduledLoaderDatabase storageHandler;

    public NpmScheduledLoaderController(
            NpmScheduledLoaderService loaderService,
            NpmScheduledLoaderDatabase storageHandler) {
        this.loaderService = loaderService;
        this.storageHandler = storageHandler;
    }

    @GetMapping("start")
    public boolean start(){
        loaderService.start();
        return true;
    }

    @GetMapping("pause")
    public boolean pause(){
        loaderService.pause();
        return true;
    }

    @GetMapping("dependencies")
    public List<ScheduledDependency> getDependencies(){
        return storageHandler.getDependencies();
    }

    @PutMapping("dependencies")
    public List<Boolean> putDependencies(@RequestBody List<String> dependencies){
        final List<Boolean> results = new ArrayList<>(dependencies.size());
        for(String dependency : dependencies) {
            results.add(storageHandler.putDependency(dependency));
        }
        return results;
    }

    @DeleteMapping("dependencies")
    public List<Boolean> deleteDependencies(@RequestBody List<String> dependencies){
        final List<Boolean> results = new ArrayList<>(dependencies.size());
        for(String dependency : dependencies) {
            results.add(storageHandler.deleteDependency(dependency));
        }
        return results;
    }

}