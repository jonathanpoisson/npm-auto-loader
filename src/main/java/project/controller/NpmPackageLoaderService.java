package project.controller;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import project.filesystem.FileSystemProvider;
import project.loader.NpmPackageLoader;
import project.model.CompletedUpdate;
import project.model.DependencyRequest;
import project.model.Update;
import project.storage.NpmPackageStorageHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Controller
public class NpmPackageLoaderService {

    // Keeps the completed FileSystems for 5 minutes after creation.
    private final Cache<String, NpmPackageStorageHandler> fsCache = CacheBuilder.newBuilder()
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build();

    // Broadcasts various updates to websocket listeners
    private final SimpMessagingTemplate template;

    // Provides an implementation of a FileSystemInstance
    private final FileSystemProvider fileSystemProvider;

    @Autowired
    public NpmPackageLoaderService(SimpMessagingTemplate template, FileSystemProvider fileSystemProvider) {
        this.template = template;
        this.fileSystemProvider = fileSystemProvider;
    }

    /**
     * Returns a zip file containing the Verdaccio file structure of
     * all requested dependencies.
     * @param id Session ID from a request (see below)
     * @return InputStreamResource Zip file
     * @throws IOException if the FileSystem expired
     */
    @RequestMapping("/download")
    public ResponseEntity<InputStreamResource> download(
            @RequestParam(value = "id") String id) throws IOException {

        NpmPackageStorageHandler storageHandler = fsCache.getIfPresent(id);
        if(storageHandler == null){
            return ResponseEntity.notFound().build();
        }else {
            try {
                final byte[] exported = storageHandler.export();
                final InputStreamResource resource = new InputStreamResource(
                        new ByteArrayInputStream(exported)
                );
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=storage.zip")
                        .contentLength(exported.length)
                        .contentType(MediaType.parseMediaType("application/octet-stream"))
                        .body(resource);
            } finally {
                fsCache.invalidate(id);
            }
        }
    }

    /**
     * This method is pretty complex...it is used for registration in the sense
     * that whenever a StompJS client calls it, they will automatically receive updates
     * from the SimpleMessagingTemplate (as long as they subscribe to the topic).
     *
     * Registration requires a set of dependencies to kick off the process
     * but once the call is made, the rest is automated.
     *
     * @param incoming Registration ID and Set of string representing dependencies ['verdaccio:latest', '@angular/cli']
     */
    @MessageMapping("/register")
    @SendToUser("/topic/loading/{uid}")
    public void registerPackages(String incoming) throws IOException {
        final DependencyRequest request = new Gson().fromJson(incoming, DependencyRequest.class);
        final NpmPackageStorageHandler storageHandler = new NpmPackageStorageHandler(fileSystemProvider.getInstance());
        final ConcurrentLinkedQueue<Update> queue = new ConcurrentLinkedQueue<>();
        final NpmPackageLoader loader = new NpmPackageLoader(storageHandler, queue);

        final AtomicBoolean isDone = new AtomicBoolean();
        final AtomicBoolean isQueueFlushed = new AtomicBoolean();

        new Thread(() -> {
            while(!isDone.get() && !isQueueFlushed.get()) {
                final Update message = queue.poll();
                if(message != null) {
                    this.template.convertAndSend(
                            "/topic/loading/" + request.getRegistrationID(), message
                    );
                }
                isQueueFlushed.set(!isDone.get() && isQueueFlushed.get());
            }
        }).start();

        loader.start(request);
        isDone.set(true);
        this.fsCache.put(storageHandler.getKey(), storageHandler);
        final CompletedUpdate completed = new CompletedUpdate("/download?id=" + storageHandler.getKey());
        this.template.convertAndSend("/topic/loading/" + request.getRegistrationID(), completed);
    }

}