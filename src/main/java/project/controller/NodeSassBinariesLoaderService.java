package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import project.filesystem.FileSystemInstance;
import project.filesystem.FileSystemProvider;
import project.loader.NodeSassBinariesLoader;
import project.storage.NodeSassBinariesStorageHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
public class NodeSassBinariesLoaderService {

    private final FileSystemProvider fileSystemProvider;

    @Autowired
    public NodeSassBinariesLoaderService(FileSystemProvider fileSystemProvider) {
        this.fileSystemProvider = fileSystemProvider;
    }

    /**
     * Returns a zip file containing node-sass binaries
     * @param version Version of the requested node-sass binaries
     * @return InputStreamResource Zip file
     * @throws IOException if the FileSystem or HTTP requests fail
     */
    @RequestMapping("/node-sass/{version}")
    public ResponseEntity<InputStreamResource> download(
            @PathVariable(value = "version") String version) throws IOException {
        final FileSystemInstance instance = fileSystemProvider.getInstance(
                "node-sass-" + System.currentTimeMillis()
        );
        final NodeSassBinariesStorageHandler storageHandler = new NodeSassBinariesStorageHandler(instance);
        final NodeSassBinariesLoader loader = new NodeSassBinariesLoader();
        for(String link : loader.getDownloadLinks(version)){
            final String name = link.substring(link.lastIndexOf('/'));
            storageHandler.addFile(name, loader.downloadLink(link));
        }
        final byte[] exported = storageHandler.export();
        final InputStreamResource resource = new InputStreamResource(
                new ByteArrayInputStream(exported)
        );
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=node-sass-binaries-" + version + ".zip")
                .contentLength(exported.length)
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
}