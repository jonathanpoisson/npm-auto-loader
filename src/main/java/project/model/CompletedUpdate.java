package project.model;

public class CompletedUpdate extends Update {

    private final String url;

    public CompletedUpdate(String url) {
        super(Type.COMPLETED);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
