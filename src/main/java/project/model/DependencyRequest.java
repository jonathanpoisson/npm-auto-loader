package project.model;

import java.util.Set;

public class DependencyRequest {

    // Pseudo unique ID for a given request (WebSocket route ID)
    private final String registrationID;

    // List of top-level dependencies to fetch
    private final Set<String> dependencies;

    /* Number of versions to grab from latest,
       in addition to the requested one.
       Example: @1.5.0 + 5 most recent
     */
    private final int numVersions;

    public DependencyRequest(String registrationID, Set<String> dependencies, int numVersions) {
        this.registrationID = registrationID;
        this.dependencies = dependencies;
        this.numVersions = numVersions;
    }

    public String getRegistrationID() {
        return registrationID;
    }

    public Set<String> getDependencies() {
        return dependencies;
    }

    public int getNumVersions() {
        return numVersions;
    }
}
