package project.model;

public class ErrorUpdate extends Update {

    private final String dependency;
    private final String error;

    public ErrorUpdate(String dependency, String error) {
        super(Type.ERROR);
        this.dependency = dependency;
        this.error = error;
    }

    public String getDependency() {
        return dependency;
    }

    public String getError() {
        return error;
    }
}
