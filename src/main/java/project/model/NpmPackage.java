package project.model;

import java.util.Objects;
import java.util.Set;

// POJO of the response from https://registry.npmjs.org/<package-name>/<package-version>
public class NpmPackage {

    // Name of the Package
    private final String name;

    // version of the Package
    private final String version;

    // List of dependencies for this Package
    private final Set<String> dependencies;

    // Link to the tarball for this Package
    private final String tarball;

    public NpmPackage(String name, String version, Set<String> dependencies, String tarball) {
        this.name = name;
        this.version = version;
        this.dependencies = dependencies;
        this.tarball = tarball;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public Set<String> getDependencies() {
        return dependencies;
    }

    public String getTarball() {
        return tarball;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NpmPackage that = (NpmPackage) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, version);
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ",\n version='" + version + '\'' +
                ",\n dependencies=" + dependencies +
                ",\n tarball='" + tarball + "\n";
    }
}
