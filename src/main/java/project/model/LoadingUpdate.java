package project.model;

public class LoadingUpdate extends Update {

    private final String dependency;
    private final String resolved;

    public LoadingUpdate(String dependency, String resolved) {
        super(Type.LOADING);
        this.dependency = dependency;
        this.resolved = resolved;
    }

    public String getDependency() {
        return dependency;
    }

    public String getResolved() {
        return resolved;
    }
}
