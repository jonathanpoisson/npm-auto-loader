package project.model;

public abstract class Update {

    protected enum Type{LOADING, ERROR, COMPLETED}

    private final Type type;

    Update(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }
}
