package project.model.jdbc;

import java.util.Date;
import java.util.Objects;

public class ScheduledDependency {

    private final String name;
    private final Date added;

    ScheduledDependency(String name, Date added) {
        this.name = name;
        this.added = added;
    }

    public String getName() {
        return name;
    }

    public Date getAdded() {
        return added;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScheduledDependency that = (ScheduledDependency) o;
        return  Objects.equals(name, that.name) &&
                Objects.equals(added, that.added);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, added);
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", added=" + added +
                '}';
    }
}
