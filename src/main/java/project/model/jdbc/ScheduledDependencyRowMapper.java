package project.model.jdbc;

import org.springframework.jdbc.core.RowMapper;

import javax.annotation.Nullable;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ScheduledDependencyRowMapper implements RowMapper<ScheduledDependency> {

    public static final ScheduledDependencyRowMapper INSTANCE = new ScheduledDependencyRowMapper();

    private ScheduledDependencyRowMapper(){}

    @Override
    public ScheduledDependency mapRow(@Nullable ResultSet rs, int rowNum) throws SQLException {
        if(null == rs){
            throw new SQLException("ResultSet is null.");
        }

        final String name = rs.getString("NAME");
        final long added = rs.getLong("ADDED");
        return new ScheduledDependency(name, new Date(added));
    }
}
