package project.model.jdbc;

public class ScheduledDependencyTable {

    private ScheduledDependencyTable(){}

    private static final String TABLE = "DEPENDENCIES";

    public static final String CREATE_TABLE = "" +
            "CREATE TABLE IF NOT EXISTS " + TABLE +
            "(NAME VARCHAR(512) PRIMARY KEY, ADDED NUMBER(13,0))";

    public static final String SELECT_ALL = "SELECT NAME, ADDED FROM " + TABLE;

    public static final String INSERT = "INSERT INTO " + TABLE + "(NAME, ADDED) VALUES (?,?)";

    public static final String DELETE = "DELETE FROM " + TABLE + " WHERE NAME = ?";

}
