
export const LOADING = "LOADING";
export const ERROR = "ERROR";
export const COMPLETED = "COMPLETED";

export abstract class Update{
  constructor(protected _type : string){}
  get type(): string {
    return this._type;
  }
}
export class LoadingUpdate extends Update{
  constructor(private _dependency : string, private _resolved : string){
      super(LOADING);
  }
  get dependency(): string {
    return this._dependency;
  }
  get resolved(): string {
    return this._resolved;
  }
}
export class CompletedUpdate extends Update{
  constructor(private _url : string){
    super(COMPLETED);
  }
  get url(): string {
    return this._url;
  }
}
export class ErrorUpdate extends Update{
  constructor(private _dependency : string, private _error : string){
    super(ERROR);
  }
  get dependency(): string {
    return this._dependency;
  }
  get error(): string {
    return this._error;
  }
}
