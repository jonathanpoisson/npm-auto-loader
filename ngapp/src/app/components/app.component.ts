import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import {COMPLETED, CompletedUpdate, ERROR, ErrorUpdate, LOADING, LoadingUpdate} from "../models/update";
import {FileSystemFileEntry, UploadEvent} from 'ngx-file-drop';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements AfterViewInit{

  private registrationID : string = "" + Math.floor(Math.random() * 10000);
  private stompClient;

  isLoadingMode : boolean = false;
  isLoading : boolean = false;
  updates : LoadingUpdate[] = [];
  errors : ErrorUpdate[] = [];
  dependencies : string[] = [];
  dependency : string = '';
  numVersions : number = 0;
  processedList : HTMLElement;

  ngAfterViewInit(): void {
    this.processedList = document.getElementById("processed-dependencies");
  }


  constructor(private ref : ChangeDetectorRef) {
    this.stompClient = Stomp.over(new SockJS("/socket"));
    this.subscribe();
  }

  subscribe(){
    let that  = this;
    let url = "/topic/loading/" + this.registrationID;

    let interval = setInterval(() => {
      that.ref.markForCheck();
    },500);

    this.stompClient.connect({}, function() {
      that.stompClient.subscribe(url, (response) => {
        let json : any = JSON.parse(response.body);
        switch (json['type']){
          case LOADING:
            that.showLoadingUpdate(json);
            break;
          case ERROR:
            that.showErrorUpdate(json);
            break;
          case COMPLETED:
            that.showCompletedUpdate(json);
            clearInterval(interval);
            break;
        }
      });
    });
  }

  showLoadingUpdate(update : LoadingUpdate){
    let elem : HTMLLIElement = document.createElement("li");
    elem.textContent = update.resolved;
    elem.className = "loading";
    this.processedList.appendChild(elem);
  }

  showErrorUpdate(update : ErrorUpdate){
    this.errors.push(update);
    this.ref.markForCheck();
  }

  showCompletedUpdate(update : CompletedUpdate){
    this.isLoading = false;
    this.ref.markForCheck();
    this.downloadDependencies(update.url)
  }

  reset(){
    window.location.reload();
  }

  startLoading(){
    if(this.dependencies.length > 0) {
      this.isLoadingMode = true;
      this.isLoading = true;
      this.updates.length = 0;
      this.errors.length = 0;

      let request = {
        registrationID : this.registrationID,
        dependencies : this.dependencies,
        numVersions : this.numVersions
      };

      this.stompClient.send("/ws/register", {}, JSON.stringify(request));
      this.ref.markForCheck();
    }
  }

  downloadDependencies(url: string){
    window.open(url, "_blank");
    this.ref.markForCheck();
  }

  fileDropped(event: UploadEvent) {
    for (const file of event.files) {
      if(file.fileEntry.isFile){
        let entry : FileSystemFileEntry = <FileSystemFileEntry>file.fileEntry;
        entry.file(info => {
          let reader : FileReader = new FileReader();
          reader.addEventListener("load", (loaded) => this.onFileLoaded(loaded));
          reader.readAsText(info, "utf-8");
        });
      }
    }
  }

  addDependency(){
    let value = this.dependency;
    if(value.length > 0 && this.dependencies.indexOf(value) == -1) {
      this.dependencies.push(this.dependency);
      this.dependency = '';
      this.ref.markForCheck();
    }
  }

  removeDependency(dep : string){
    let index : number = this.dependencies.indexOf(dep);
    if(index > -1){
      this.dependencies.splice(index, 1);
      this.ref.markForCheck();
    }
  }

  downloadBindings(version : string){
    window.open("/node-sass/" + version, "_blank");
  }

  private onFileLoaded(event : Event){
    let content = JSON.parse(event.target["result"]);
    let deps = Object.assign(content['dependencies'], content['devDependencies']);
    this.addDependencies(deps);
  }

  private addDependencies(json : Object){
    let keys = Object.keys(json);
    for(let i = 0 ; i < keys.length; i++){
      let key : string = keys[i];
      this.dependencies.push(key +':' + json[key]);
    }
    this.ref.detectChanges();
  }

}
