import {AfterViewChecked, Directive, ElementRef} from '@angular/core';


@Directive({ selector: '[scrollToBottom]' })
export class ScrollToBottomDirective implements AfterViewChecked {
    constructor(private element:ElementRef) {}
    ngAfterViewChecked(): void {
      if(this.element != undefined){
        let height : number = this.element.nativeElement.scrollHeight;
        this.element.nativeElement.scrollTop = height;
      }
    }
}
